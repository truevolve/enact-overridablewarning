package com.truevolve.android.enact.controllers.overridablewarning

/**
 * Created by developer on 3/15/17.
 */

class OverrideResult {

    private val type = "override_warning"

    var result: String? = null
    var message: String? = null
    var titleText: String? = null
    var messageText: String? = null
    var explanationText: String? = null

    constructor() {}

    constructor(result: String?, message: String?, titleText: String?, messageText: String?, explanationText: String?) {
        this.result = result
        this.message = message
        this.titleText = titleText
        this.messageText = messageText
        this.explanationText = explanationText
    }

    fun getType(): String {
        return type
    }

}
