package com.truevolve.android.enact.controllers.overridablewarning

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.truevolve.enact.Interpreter
import com.truevolve.enact.controllers.ActivityBaseController
import com.truevolve.enact.exceptions.ObjectNotFoundException
import com.truevolve.enact.exceptions.PolicyException
import ng.max.slideview.SlideView
import org.json.JSONException
import org.json.JSONObject
import java.lang.reflect.InvocationTargetException

class OverridableWarning : ActivityBaseController() {

    private val TAG = "OverridableWarning"
    private val STORE_AS = "store_as"
    private val TITLE_TEXT = "title_text"
    private val MESSAGE_TEXT = "message_text"
    private val EXPLANATION_TEXT = "explanation_text"
    private val ON_OVERRIDE = "on_override"
    private val SLIDER_TEXT = "slider_text"
    private val OVERRIDE_KEY = "override_key"
    private val OVERRIDE_MESSAGE = "override_message"
    private val ON_OK = "on_ok"
    private val OK_BUTTON_TEXT = "ok_button_text"
    private val OK_KEY = "ok_key"
    private val OK_MESSAGE = "ok_message"
    private val RETRIEVE_AS = "retrieve_as"

    private val CONTROLLER_TYPE = "overrideable_warning"

    private var storeAs: String? = null
    private var onOverride: String? = null
    private var overrideKey: String? = null
    private var overrideMessage: String? = null
    private var onOk: String? = null
    private var okKey: String? = null
    private var okMessage: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_override)

        try {
            storeAs = stateObj?.getString(STORE_AS)

            val titleText = stateObj?.getString(TITLE_TEXT)
            val messageText = stateObj?.getString(MESSAGE_TEXT)

            val explanationText = if (stateObj?.has(RETRIEVE_AS) == true) {
                //We have to look for a list comment.
                val data = Interpreter.dataStore
                val explanationTextFromPolicy = stateObj?.getString(RETRIEVE_AS)?.let {
                    data.getProperty<String>(it)
                } ?: ""
                explanationTextFromPolicy

            } else {
                //We'll display the explanation text from the policy.
                stateObj?.getString(EXPLANATION_TEXT) ?: ""
            }

            val sliderText = stateObj?.getString(SLIDER_TEXT)
            val okButtonText = stateObj?.getString(OK_BUTTON_TEXT)

            onOverride = stateObj?.getString(ON_OVERRIDE)
            overrideKey = stateObj?.getString(OVERRIDE_KEY)
            overrideMessage = stateObj?.getString(OVERRIDE_MESSAGE)

            onOk = stateObj?.getString(ON_OK)
            okKey = stateObj?.getString(OK_KEY)
            okMessage = stateObj?.getString(OK_MESSAGE)

            val titleTextView = findViewById<View>(R.id.title_text) as TextView
            titleTextView.text = titleText

            val messageTextView = findViewById<View>(R.id.message_text) as TextView
            messageTextView.text = messageText

            val explanationTextView = findViewById<View>(R.id.explanation_text) as TextView
            explanationTextView.text = explanationText

            val slideView = findViewById<View>(R.id.slideView) as SlideView
            slideView.setText(sliderText)
            slideView.setOnSlideCompleteListener {
                try {
                    val overrideResult = OverrideResult(overrideKey, overrideMessage, titleText, messageText, explanationText)
                    storeAs?.let { Interpreter.dataStore.put(it, overrideResult) }
                    onOverride?.let { goToState(it) }

                } catch (e: Exception) {
                    e.printStackTrace()
                    Interpreter.error(this@OverridableWarning, e)
                }
            }

            val overrideButton = findViewById<View>(R.id.buttonOverride) as Button
            overrideButton.setOnClickListener {
                overrideButton.visibility = View.GONE
                slideView.visibility = View.VISIBLE
            }

            val buttonOk = findViewById<View>(R.id.buttonOk) as Button
            buttonOk.text = okButtonText
            buttonOk.setOnClickListener {
                try {
                    val overrideResult = OverrideResult(okKey, okMessage, titleText, messageText, explanationText)
                    storeAs?.let { Interpreter.dataStore.put(it, overrideResult) }

                    onOk?.let { goToState(it) }

                } catch (e: Exception) {
                    e.printStackTrace()
                    Interpreter.error(this@OverridableWarning, e)
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override val type = CONTROLLER_TYPE

    @Throws(PolicyException::class)
    override fun validate(stateObj: JSONObject) {
        Log.d(TAG, "validate: starting")

        if (!stateObj.has(STORE_AS)) {
            Log.e(TAG, "validate: state object must have $STORE_AS defined")
            throw PolicyException("State object must have $STORE_AS defined")
        } else if (!stateObj.has(TITLE_TEXT)) {
            Log.e(TAG, "validate: state object must have $TITLE_TEXT defined")
            throw PolicyException("State object must have $TITLE_TEXT defined")
        } else if (!stateObj.has(MESSAGE_TEXT)) {
            Log.e(TAG, "validate: state object must have $MESSAGE_TEXT defined")
            throw PolicyException("State object must have $MESSAGE_TEXT defined")
        } else if (!stateObj.has(EXPLANATION_TEXT)) {
            Log.e(TAG, "validate: state object must have $EXPLANATION_TEXT defined")
            throw PolicyException("State object must have $EXPLANATION_TEXT defined")
        } else if (!stateObj.has(ON_OVERRIDE)) {
            Log.e(TAG, "validate: state object must have $ON_OVERRIDE defined")
            throw PolicyException("State object must have $ON_OVERRIDE defined")
        } else if (!stateObj.has(SLIDER_TEXT)) {
            Log.e(TAG, "validate: state object must have $SLIDER_TEXT defined")
            throw PolicyException("State object must have $SLIDER_TEXT defined")
        } else if (!stateObj.has(OVERRIDE_KEY)) {
            Log.e(TAG, "validate: state object must have $OVERRIDE_KEY defined")
            throw PolicyException("State object must have $OVERRIDE_KEY defined")
        } else if (!stateObj.has(OVERRIDE_MESSAGE)) {
            Log.e(TAG, "validate: state object must have $OVERRIDE_MESSAGE defined")
            throw PolicyException("State object must have $OVERRIDE_MESSAGE defined")
        } else if (!stateObj.has(ON_OK)) {
            Log.e(TAG, "validate: state object must have $ON_OK defined")
            throw PolicyException("State object must have $ON_OK defined")
        } else if (!stateObj.has(OK_BUTTON_TEXT)) {
            Log.e(TAG, "validate: state object must have $OK_BUTTON_TEXT defined")
            throw PolicyException("State object must have $OK_BUTTON_TEXT defined")
        } else if (!stateObj.has(OK_KEY)) {
            Log.e(TAG, "validate: state object must have $OK_KEY defined")
            throw PolicyException("State object must have $OK_KEY defined")
        } else if (!stateObj.has(OK_MESSAGE)) {
            Log.e(TAG, "validate: state object must have $OK_MESSAGE defined")
            throw PolicyException("State object must have $OK_MESSAGE defined")
        }
    }
}
