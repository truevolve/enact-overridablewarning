package com.truevolve.android.enact.controllers.sampleoverride;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.truevolve.android.enact.controllers.overridablewarning.OverridableWarning;
import com.truevolve.enact.Interpreter;
import com.truevolve.enact.controllers.ActivityBaseController;
import com.truevolve.enact.exceptions.InterpreterException;
import com.truevolve.enact.exceptions.PolicyException;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private String policy =
            "{\n" +
                    "  \"start\": {\n" +
                    "    \"type\": \"menu\",\n" +
                    "    \"on_start\": \"get_warning\"\n" +
                    "  },\n" +
                    "  \"get_warning\": {\n" +
                    "    \"type\": \"overrideable_warning\",\n" +
                    "    \"store_as\": \"override_result\",\n" +
                    "    \n" +
                    "    \"title_text\":\"Warning!\",\n" +
                    "    \"message_text\": \"This is not allowed.\",\n" +
                    "    \"explanation_text\":\"Choose to OVERRIDE this warning and allow the process to continue or pres OK to end the process now.\",\n" +
                    "    \n" +
                    "    \"on_override\" :\"summary\",\n" +
                    "    \"slider_text\":\"Slide to override\",\n" +
                    "    \"override_key\":\"WARNING OVERRIDDEN\",\n" +
                    "    \"override_message\":\"You have chosen to override the warning given to you.\",\n" +
                    "    \n" +
                    "    \"on_ok\" :\"summary\",\n" +
                    "    \"ok_button_text\":\"Ok, do not override\",\n" +
                    "    \"ok_key\":\"ADHERED TO WARNING\",\n" +
                    "    \"ok_message\":\"The process is now completed.\"\n" +
                    "  },\n" +
                    "  \"summary\": {\n" +
                    "    \"type\": \"summary\",\n" +
                    "    \"on_done\": \"end\",\n" +
                    "    \"override_result\": \"override_result\"\n" +
                    "  }\n" +
                    "}";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (getIntent().hasExtra(Interpreter.END)) {
            Log.d(TAG, "onCreate: ENDED");
        }
        if (getIntent().hasExtra(Interpreter.ERROR)) {
            Log.d(TAG, "onCreate: ERRORED");
            Throwable throwable = (Throwable) getIntent().getSerializableExtra("exception");
            Log.e(TAG, "onCreate: Errored: ", throwable);
        }

        List<Class<? extends ActivityBaseController>> listOfControllers = new ArrayList<>();
        listOfControllers.add(Menu.class);
        listOfControllers.add(OverridableWarning.class);
        listOfControllers.add(Summary.class);

        try {
            Interpreter interpreter = Interpreter.INSTANCE.setup(MainActivity.class, policy, listOfControllers);
            interpreter.start(this);


        } catch (JSONException | PolicyException | InterpreterException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: RESUMING");
    }
}
